import * as React from "react";
import { Routes, Route } from "react-router-dom";
import "./App.css";
import Login from "./components/login";
import Register from "./components/register";
import Landing from "./components/landing";

function App() {
  return (
    <div className="App">
      <Routes>
        <Route exact path="/" element={<Register />}></Route>
        <Route path="login" element={<Login />}></Route>
        <Route path="landing" element={<Landing />}></Route>
      </Routes>
    </div>
  );
}

export default App;

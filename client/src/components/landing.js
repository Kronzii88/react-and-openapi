import React, { Component } from "react";
import { Container } from "react-bootstrap";
import "./style.css";

class Landing extends Component {
  render() {
    return (
      <Container>
        <h1>Landing</h1>
      </Container>
    );
  }
}

export default Landing;

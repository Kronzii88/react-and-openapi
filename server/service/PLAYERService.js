'use strict';


/**
 * create player own biodata
 * use this to create the user biodata
 *
 * returns inline_response_200_2
 **/
exports.createBiodata = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "address" : "jalan sesama",
    "updated_at" : "11-12-2021 12:00 +07:00",
    "avatarUrl" : "image/avatar.png",
    "phone" : "089238471283",
    "bio" : "biodata lengkap pemain",
    "created_at" : "10-12-2021 12:00 +07:00",
    "id" : "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
    "userId" : "046b6c7f-0b8a-43b9-b35d-6489e6daee91"
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * player create room fo play
 * use this to create room for players to play
 *
 * body Object 
 * returns inline_response_200_4
 **/
exports.createRoom = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "roundCount" : 2,
    "updated_at" : "11-12-2021 12:00 +07:00",
    "winnerId" : "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
    "name" : "room gg",
    "created_at" : "10-12-2021 12:00 +07:00",
    "id" : "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
    "status" : "OPEN"
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * get all games that have been played
 * use this endpoint to get all games from database
 *
 * returns inline_response_200_4
 **/
exports.getPlayedGames = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "roundCount" : 2,
    "updated_at" : "11-12-2021 12:00 +07:00",
    "winnerId" : "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
    "name" : "room gg",
    "created_at" : "10-12-2021 12:00 +07:00",
    "id" : "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
    "status" : "OPEN"
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * create player own biodata
 * use this to get their own biodata player
 *
 * returns inline_response_200_2
 **/
exports.getPlayerBiodata = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "address" : "jalan sesama",
    "updated_at" : "11-12-2021 12:00 +07:00",
    "avatarUrl" : "image/avatar.png",
    "phone" : "089238471283",
    "bio" : "biodata lengkap pemain",
    "created_at" : "10-12-2021 12:00 +07:00",
    "id" : "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
    "userId" : "046b6c7f-0b8a-43b9-b35d-6489e6daee91"
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * get all rooms information with status open
 * use this to retrieve all rooms information
 *
 * returns inline_response_200_4
 **/
exports.getPlayerRoom = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "roundCount" : 2,
    "updated_at" : "11-12-2021 12:00 +07:00",
    "winnerId" : "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
    "name" : "room gg",
    "created_at" : "10-12-2021 12:00 +07:00",
    "id" : "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
    "status" : "OPEN"
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * get all round information
 * use this to retrieve all round information
 *
 * returns inline_response_200_5
 **/
exports.getPlayerRound = function() {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "updated_at" : "11-12-2021 12:00 +07:00",
    "winnerId" : "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
    "created_at" : "10-12-2021 12:00 +07:00",
    "roomId" : "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
    "status" : "PLAYER_1_PICK"
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * pick a suit
 * players make turn to play
 *
 * body Object 
 * returns inline_response_200_3
 **/
exports.v1Player_pickPOST = function(body) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "data" : {
    "inferiorId" : 3,
    "updated_at" : "11-12-2021 12:00 +07:00",
    "name" : "Rock",
    "created_at" : "10-12-2021 12:00 +07:00",
    "id" : 1,
    "superiorId" : 2
  },
  "status" : "OK"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


'use strict';

var utils = require('../utils/writer.js');
var PLAYER = require('../service/PLAYERService');

module.exports.createBiodata = function createBiodata (req, res, next) {
  PLAYER.createBiodata()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.createRoom = function createRoom (req, res, next, body) {
  PLAYER.createRoom(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getPlayedGames = function getPlayedGames (req, res, next) {
  PLAYER.getPlayedGames()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getPlayerBiodata = function getPlayerBiodata (req, res, next) {
  PLAYER.getPlayerBiodata()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getPlayerRoom = function getPlayerRoom (req, res, next) {
  PLAYER.getPlayerRoom()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getPlayerRound = function getPlayerRound (req, res, next) {
  PLAYER.getPlayerRound()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.v1Player_pickPOST = function v1Player_pickPOST (req, res, next, body) {
  PLAYER.v1Player_pickPOST(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

'use strict';

var utils = require('../utils/writer.js');
var Admin = require('../service/AdminService');

module.exports.getBiodata = function getBiodata (req, res, next) {
  Admin.getBiodata()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getGames = function getGames (req, res, next) {
  Admin.getGames()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getPlayers = function getPlayers (req, res, next) {
  Admin.getPlayers()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getRoom = function getRoom (req, res, next) {
  Admin.getRoom()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getRound = function getRound (req, res, next) {
  Admin.getRound()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
